# Project: CI/CD Implementation with GitLab CI/CD Pipelines

This repository contains code for a project implementing CI/CD (Continuous Integration/Continuous Deployment) using GitLab CI/CD pipelines. The project builds a static site using Eleventy, tests the artifact, and deploys it to Surge.sh. Below are the details of each stage:

## Stages

- **Build**: Building the Eleventy Static Site.
- **Test**: Testing the Artifact.
- **Deploy**: Deploying to Surge.sh.

## Build Stage

In this stage, the Eleventy static site is built using Node.js LTS Docker image. The following steps are executed:

1. Install dependencies.
2. Install Eleventy.
3. Generate the site.

The resulting artifacts are stored in the `_site/` directory.

## Test Stage

In this stage, the generated site is tested. It checks if the string "Hi" exists in the `index.html` file.

## Deploy Stage

In this stage, the built site is deployed to Surge.sh. The steps include:

1. Installing Surge globally.
2. Deploying the site to a Surge domain.

## Execution Flow

The CI/CD pipeline executes in the following sequence:

1. Build stage
2. Test stage
3. Deploy stage

For each stage, defined scripts are executed in their respective environments.

For more details on the pipeline configuration, refer to the `.gitlab-ci.yml` file.
